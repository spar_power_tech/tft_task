#line 1 "C:/Users/James/Desktop/SPARR/SPAR/Program/Demo_touch_back_up - Copy/mikroC/MyProject_events_code.c"
#line 1 "c:/users/james/desktop/sparr/spar/program/demo_touch_back_up - copy/mikroc/myproject_objects.h"
typedef enum {_taLeft, _taCenter, _taRight} TTextAlign;
typedef enum {_tavTop, _tavMiddle, _tavBottom} TTextAlignVertical;

typedef struct Screen TScreen;

typedef struct {
 unsigned int X_Min;
 unsigned int X_Max;
 unsigned int Y_Min;
 unsigned int Y_Max;
 char Rotate;
} TTPConstants;

typedef struct Button {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 char *Caption;
 TTextAlign TextAlign;
 TTextAlignVertical TextAlignVertical;
 const far char *FontName;
 unsigned int Font_Color;
 char VerticalText;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TButton;

typedef struct Image {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 const far char *Picture_Name;
 char Visible;
 char Active;
 char Picture_Type;
 char Picture_Ratio;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TImage;

struct Screen {
 unsigned int Color;
 unsigned int Width;
 unsigned int Height;
 unsigned int ObjectsCount;
 unsigned int ButtonsCount;
 TButton * const code far *Buttons;
 unsigned int ImagesCount;
 TImage * const code far *Images;
};

extern TScreen Screen1;
extern TImage Image1;
extern TImage * const code far Screen1_Images[1];

extern TScreen Screen2;
extern TButton Button1;
extern TButton Button2;
extern TButton Button3;
extern TButton Button4;
extern TButton Button5;
extern TButton Button6;
extern TButton * const code far Screen2_Buttons[6];




void Button1OnClick();
void Button2Onclick();
void Button3Onpush();
void Button3Onrelease();
void Button4Onpush();
void Button4Onrelease();
void Button5Onpush();
void Button5Onrelease();
void Button6Onclick();
void Image1Onclick();




extern char Image1_Caption[];
extern char Button1_Caption[];
extern char Button2_Caption[];
extern char Button3_Caption[];
extern char Button4_Caption[];
extern char Button5_Caption[];
extern char Button6_Caption[];


void DrawScreen(TScreen *aScreen);
void DrawButton(TButton *aButton);
void DrawImage(TImage *AImage);
void Check_TP();
void Start_TP();
void Process_TP_Press(unsigned int X, unsigned int Y);
void Process_TP_Up(unsigned int X, unsigned int Y);
void Process_TP_Down(unsigned int X, unsigned int Y);
#line 1 "c:/users/james/desktop/sparr/spar/program/demo_touch_back_up - copy/mikroc/myproject_resources.h"
const code far char Tahoma11x13_Regular[];
const code far char all_jpg[13852];
#line 12 "C:/Users/James/Desktop/SPARR/SPAR/Program/Demo_touch_back_up - Copy/mikroC/MyProject_events_code.c"
unsigned short testvar = 0x01;
extern void EXPANDER_write(char chipxx, char portAddr, char dat);
static unsigned short maskstart = 0x03;
static unsigned short maskjog = 0x04;
static unsigned short maskcrawl = 0x08;
static unsigned short maskspeedup = 0x10;
static unsigned short maskspeedown = 0x20;
static unsigned short maskcoast = 0x40;

void Button1OnClick() {
 testvar ^= maskstart;


 EXPANDER_write( 0x40 , 0x14,testvar);
 if ((testvar & maskstart) == 0x01)
 {
 Button1.Gradient_Start_Color = 0xF800;
 Button1.Caption = "Stop";
 DrawButton(&Button1);
 Button2.Active = 0;
 Button3.Active = 0;
 }
 else
 {
 Button1.Gradient_Start_Color = 0x67E0;
 Button1.Caption = "Start";
 DrawButton(&Button1);
 Button6.Gradient_Start_Color = 0x6013;
 DrawButton(&Button6);
 Button2.Active = 1;
 Button3.Active = 1;
 }
}

void Button2Onclick() {
 testvar ^= maskjog;

 EXPANDER_write( 0x40 , 0x14,testvar);
 if ((testvar & maskjog) == maskjog)
 {
 Button2.Gradient_Start_Color = 0xF800;
 Button2.Caption = "Stop Jog";
 DrawButton(&Button2);
 Button6.Gradient_Start_Color = 0x6013;
 DrawButton(&Button6);
 Button4.Active = 0;
 Button5.Active = 0;
 }
 else if ((testvar & maskjog) != maskjog)
 {
 Button2.Gradient_Start_Color = 0xCE66;
 Button2.Caption = "Jog";
 DrawButton(&Button2);
 Button6.Gradient_Start_Color = 0x6013;
 DrawButton(&Button6);
 Button4.Active = 1;
 Button5.Active = 1;
 }
}

void Button3Onpush() {
 testvar ^= maskcrawl;

 EXPANDER_write( 0x40 , 0x14,testvar);
 Button3.Gradient_Start_Color = 0x300C;
 DrawButton(&Button3);
 Button6.Gradient_Start_Color = 0x6013;
 DrawButton(&Button6);
}
void Button3Onrelease() {
 testvar ^= maskcrawl;

 EXPANDER_write( 0x40 , 0x14,testvar);
 Button3.Gradient_Start_Color = 0xC993;
 DrawButton(&Button3);
 Button6.Gradient_Start_Color = 0x6013;
 DrawButton(&Button6);
}

void Button4Onpush() {
 testvar ^= maskspeedup;

 EXPANDER_write( 0x40 , 0x14,testvar);
 Button4.Gradient_Start_Color = 0x3320;
 DrawButton(&Button4);
 Button6.Gradient_Start_Color = 0x6013;
 DrawButton(&Button6);
}
void Button4Onrelease() {
 testvar ^= maskspeedup;

 EXPANDER_write( 0x40 , 0x14,testvar);
 Button4.Gradient_Start_Color = 0x3660;
 DrawButton(&Button4);
 Button6.Gradient_Start_Color = 0x6013;
 DrawButton(&Button6);
}

void Button5Onpush() {
 testvar ^= maskspeedown;

 EXPANDER_write( 0x40 , 0x14,testvar);
 Button5.Gradient_Start_Color = 0xF800;
 DrawButton(&Button5);
 Button6.Gradient_Start_Color = 0x6013;
 DrawButton(&Button6);
}
void Button5Onrelease() {
 testvar ^= maskspeedown;

 EXPANDER_write( 0x40 , 0x14,testvar);
 Button5.Gradient_Start_Color = 0xC980;
 DrawButton(&Button5);
 Button6.Gradient_Start_Color = 0x6013;
 DrawButton(&Button6);
}

void Image1Onclick() {
 DrawScreen(&Screen2);
}

void Button6Onclick() {
 testvar = maskcoast;
 EXPANDER_write( 0x40 , 0x14,testvar);
 testvar = 0x01;
 Button6.Gradient_Start_Color = 0xF800;
 DrawButton(&Button6);
 Button1.Gradient_Start_Color = 0x67E0;
 Button1.Caption = "Start";
 DrawButton(&Button1);
 Button2.Gradient_Start_Color = 0xCE66;
 Button2.Caption = "Jog";
 DrawButton(&Button2);
}
