
MyProject_driver_InitializeTouchPanel:

	PUSH	W10
	PUSH	W11
	CLR	TRISD
	MOV	#272, W11
	MOV	#480, W10
	CALL	_TFT_Init_SSD1963
	MOV	#lo_addr(_PenDown), W1
	CLR	W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _PressedObject
	MOV	#65535, W0
	MOV	W0, _PressedObjectType
L_end_InitializeTouchPanel:
	POP	W11
	POP	W10
	RETURN
; end of MyProject_driver_InitializeTouchPanel

MyProject_driver_InitializeObjects:

	MOV	#23275, W0
	MOV	W0, _Screen1
	MOV	#480, W0
	MOV	W0, _Screen1+2
	MOV	#272, W0
	MOV	W0, _Screen1+4
	CLR	W0
	MOV	W0, _Screen1+8
	MOV	#1, W0
	MOV	W0, _Screen1+14
	MOV	#lo_addr(_Screen1_Images), W0
	MOV	#higher_addr(_Screen1_Images), W1
	MOV	W0, _Screen1+16
	MOV	W1, _Screen1+18
	MOV	#1, W0
	MOV	W0, _Screen1+6
	MOV	#520, W0
	MOV	W0, _Screen2
	MOV	#480, W0
	MOV	W0, _Screen2+2
	MOV	#272, W0
	MOV	W0, _Screen2+4
	MOV	#6, W0
	MOV	W0, _Screen2+8
	MOV	#lo_addr(_Screen2_Buttons), W0
	MOV	#higher_addr(_Screen2_Buttons), W1
	MOV	W0, _Screen2+10
	MOV	W1, _Screen2+12
	CLR	W0
	MOV	W0, _Screen2+14
	MOV	#6, W0
	MOV	W0, _Screen2+6
	MOV	#lo_addr(_Screen1), W0
	MOV	W0, _Image1
	MOV	#lo_addr(_Image1+2), W1
	CLR	W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Image1+4
	CLR	W0
	MOV	W0, _Image1+6
	MOV	#480, W0
	MOV	W0, _Image1+8
	MOV	#272, W0
	MOV	W0, _Image1+10
	MOV	#lo_addr(_Image1+18), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Image1+19), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_all_jpg), W0
	MOV	#higher_addr(_all_jpg), W1
	MOV	W0, _Image1+12
	MOV	W1, _Image1+14
	MOV	#lo_addr(_Image1+16), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Image1+17), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Image1+20
	CLR	W0
	MOV	W0, _Image1+22
	MOV	#lo_addr(_Image1Onclick), W0
	MOV	W0, _Image1+24
	CLR	W0
	MOV	W0, _Image1+26
	MOV	#lo_addr(_Screen2), W0
	MOV	W0, _Button1
	MOV	#lo_addr(_Button1+2), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#43, W0
	MOV	W0, _Button1+4
	MOV	#33, W0
	MOV	W0, _Button1+6
	MOV	#83, W0
	MOV	W0, _Button1+8
	MOV	#72, W0
	MOV	W0, _Button1+10
	MOV	#lo_addr(_Button1+12), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Button1+14
	MOV	#lo_addr(_Button1+16), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button1+17), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button1+18), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button1_Caption), W0
	MOV	W0, _Button1+20
	MOV	#lo_addr(_Button1+22), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button1+23), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Tahoma11x13_Regular), W2
	MOV	#higher_addr(_Tahoma11x13_Regular), W3
	MOV	W2, _Button1+24
	MOV	W3, _Button1+26
	MOV	#lo_addr(_Button1+40), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Button1+28
	MOV	#lo_addr(_Button1+30), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button1+31), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button1+32), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#26592, W0
	MOV	W0, _Button1+34
	MOV	#65535, W0
	MOV	W0, _Button1+36
	MOV	#50712, W0
	MOV	W0, _Button1+38
	MOV	#12313, W0
	MOV	W0, _Button1+42
	CLR	W0
	MOV	W0, _Button1+44
	CLR	W0
	MOV	W0, _Button1+46
	MOV	#lo_addr(_Button1OnClick), W0
	MOV	W0, _Button1+48
	CLR	W0
	MOV	W0, _Button1+50
	MOV	#lo_addr(_Screen2), W0
	MOV	W0, _Button2
	MOV	#lo_addr(_Button2+2), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#196, W0
	MOV	W0, _Button2+4
	MOV	#33, W0
	MOV	W0, _Button2+6
	MOV	#83, W0
	MOV	W0, _Button2+8
	MOV	#72, W0
	MOV	W0, _Button2+10
	MOV	#lo_addr(_Button2+12), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Button2+14
	MOV	#lo_addr(_Button2+16), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button2+17), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button2+18), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button2_Caption), W0
	MOV	W0, _Button2+20
	MOV	#lo_addr(_Button2+22), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button2+23), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	W2, _Button2+24
	MOV	W3, _Button2+26
	MOV	#lo_addr(_Button2+40), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Button2+28
	MOV	#lo_addr(_Button2+30), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button2+31), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button2+32), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#52838, W0
	MOV	W0, _Button2+34
	MOV	#50712, W0
	MOV	W0, _Button2+36
	MOV	#50712, W0
	MOV	W0, _Button2+38
	MOV	#31, W0
	MOV	W0, _Button2+42
	CLR	W0
	MOV	W0, _Button2+44
	CLR	W0
	MOV	W0, _Button2+46
	MOV	#lo_addr(_Button2Onclick), W0
	MOV	W0, _Button2+48
	CLR	W0
	MOV	W0, _Button2+50
	MOV	#lo_addr(_Screen2), W0
	MOV	W0, _Button3
	MOV	#lo_addr(_Button3+2), W1
	MOV.B	#2, W0
	MOV.B	W0, [W1]
	MOV	#349, W0
	MOV	W0, _Button3+4
	MOV	#33, W0
	MOV	W0, _Button3+6
	MOV	#83, W0
	MOV	W0, _Button3+8
	MOV	#72, W0
	MOV	W0, _Button3+10
	MOV	#lo_addr(_Button3+12), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Button3+14
	MOV	#lo_addr(_Button3+16), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button3+17), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button3+18), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button3_Caption), W0
	MOV	W0, _Button3+20
	MOV	#lo_addr(_Button3+22), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button3+23), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	W2, _Button3+24
	MOV	W3, _Button3+26
	MOV	#lo_addr(_Button3+40), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Button3+28
	MOV	#lo_addr(_Button3+30), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button3+31), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button3+32), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#51603, W0
	MOV	W0, _Button3+34
	MOV	#50712, W0
	MOV	W0, _Button3+36
	MOV	#50712, W0
	MOV	W0, _Button3+38
	MOV	#31, W0
	MOV	W0, _Button3+42
	MOV	#lo_addr(_Button3Onrelease), W0
	MOV	W0, _Button3+44
	MOV	#lo_addr(_Button3Onpush), W0
	MOV	W0, _Button3+46
	CLR	W0
	MOV	W0, _Button3+48
	CLR	W0
	MOV	W0, _Button3+50
	MOV	#lo_addr(_Screen2), W0
	MOV	W0, _Button4
	MOV	#lo_addr(_Button4+2), W1
	MOV.B	#3, W0
	MOV.B	W0, [W1]
	MOV	#43, W0
	MOV	W0, _Button4+4
	MOV	#150, W0
	MOV	W0, _Button4+6
	MOV	#83, W0
	MOV	W0, _Button4+8
	MOV	#72, W0
	MOV	W0, _Button4+10
	MOV	#lo_addr(_Button4+12), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Button4+14
	MOV	#lo_addr(_Button4+16), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button4+17), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button4+18), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button4_Caption), W0
	MOV	W0, _Button4+20
	MOV	#lo_addr(_Button4+22), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button4+23), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	W2, _Button4+24
	MOV	W3, _Button4+26
	MOV	#lo_addr(_Button4+40), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Button4+28
	MOV	#lo_addr(_Button4+30), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button4+31), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button4+32), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#13920, W0
	MOV	W0, _Button4+34
	MOV	#50712, W0
	MOV	W0, _Button4+36
	MOV	#50712, W0
	MOV	W0, _Button4+38
	MOV	#59164, W0
	MOV	W0, _Button4+42
	MOV	#lo_addr(_Button4Onrelease), W0
	MOV	W0, _Button4+44
	MOV	#lo_addr(_Button4Onpush), W0
	MOV	W0, _Button4+46
	CLR	W0
	MOV	W0, _Button4+48
	CLR	W0
	MOV	W0, _Button4+50
	MOV	#lo_addr(_Screen2), W0
	MOV	W0, _Button5
	MOV	#lo_addr(_Button5+2), W1
	MOV.B	#4, W0
	MOV.B	W0, [W1]
	MOV	#196, W0
	MOV	W0, _Button5+4
	MOV	#150, W0
	MOV	W0, _Button5+6
	MOV	#83, W0
	MOV	W0, _Button5+8
	MOV	#72, W0
	MOV	W0, _Button5+10
	MOV	#lo_addr(_Button5+12), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Button5+14
	MOV	#lo_addr(_Button5+16), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button5+17), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button5+18), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button5_Caption), W0
	MOV	W0, _Button5+20
	MOV	#lo_addr(_Button5+22), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button5+23), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	W2, _Button5+24
	MOV	W3, _Button5+26
	MOV	#lo_addr(_Button5+40), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Button5+28
	MOV	#lo_addr(_Button5+30), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button5+31), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button5+32), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#51584, W0
	MOV	W0, _Button5+34
	MOV	#50712, W0
	MOV	W0, _Button5+36
	MOV	#50712, W0
	MOV	W0, _Button5+38
	MOV	#59164, W0
	MOV	W0, _Button5+42
	MOV	#lo_addr(_Button5Onrelease), W0
	MOV	W0, _Button5+44
	MOV	#lo_addr(_Button5Onpush), W0
	MOV	W0, _Button5+46
	CLR	W0
	MOV	W0, _Button5+48
	CLR	W0
	MOV	W0, _Button5+50
	MOV	#lo_addr(_Screen2), W0
	MOV	W0, _Button6
	MOV	#lo_addr(_Button6+2), W1
	MOV.B	#5, W0
	MOV.B	W0, [W1]
	MOV	#350, W0
	MOV	W0, _Button6+4
	MOV	#150, W0
	MOV	W0, _Button6+6
	MOV	#83, W0
	MOV	W0, _Button6+8
	MOV	#72, W0
	MOV	W0, _Button6+10
	MOV	#lo_addr(_Button6+12), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Button6+14
	MOV	#lo_addr(_Button6+16), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button6+17), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button6+18), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button6_Caption), W0
	MOV	W0, _Button6+20
	MOV	#lo_addr(_Button6+22), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button6+23), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	W2, _Button6+24
	MOV	W3, _Button6+26
	MOV	#lo_addr(_Button6+40), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _Button6+28
	MOV	#lo_addr(_Button6+30), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button6+31), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button6+32), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#24595, W0
	MOV	W0, _Button6+34
	MOV	#50712, W0
	MOV	W0, _Button6+36
	MOV	#50712, W0
	MOV	W0, _Button6+38
	MOV	#59164, W0
	MOV	W0, _Button6+42
	CLR	W0
	MOV	W0, _Button6+44
	CLR	W0
	MOV	W0, _Button6+46
	MOV	#lo_addr(_Button6Onclick), W0
	MOV	W0, _Button6+48
	CLR	W0
	MOV	W0, _Button6+50
L_end_InitializeObjects:
	RETURN
; end of MyProject_driver_InitializeObjects

MyProject_driver_IsInsideObject:
	LNK	#0

; Width start address is: 2 (W1)
	MOV	[W14-8], W1
; Height start address is: 4 (W2)
	MOV	[W14-10], W2
	CP	W12, W10
	BRA LEU	L_MyProject_driver_IsInsideObject101
	GOTO	L_MyProject_driver_IsInsideObject91
L_MyProject_driver_IsInsideObject101:
	ADD	W12, W1, W0
; Width end address is: 2 (W1)
	DEC	W0
	CP	W0, W10
	BRA GEU	L_MyProject_driver_IsInsideObject102
	GOTO	L_MyProject_driver_IsInsideObject90
L_MyProject_driver_IsInsideObject102:
	CP	W13, W11
	BRA LEU	L_MyProject_driver_IsInsideObject103
	GOTO	L_MyProject_driver_IsInsideObject89
L_MyProject_driver_IsInsideObject103:
	ADD	W13, W2, W0
; Height end address is: 4 (W2)
	DEC	W0
	CP	W0, W11
	BRA GEU	L_MyProject_driver_IsInsideObject104
	GOTO	L_MyProject_driver_IsInsideObject88
L_MyProject_driver_IsInsideObject104:
L_MyProject_driver_IsInsideObject87:
	MOV.B	#1, W0
	GOTO	L_end_IsInsideObject
L_MyProject_driver_IsInsideObject91:
L_MyProject_driver_IsInsideObject90:
L_MyProject_driver_IsInsideObject89:
L_MyProject_driver_IsInsideObject88:
	CLR	W0
L_end_IsInsideObject:
	ULNK
	RETURN
; end of MyProject_driver_IsInsideObject

_DrawButton:
	LNK	#4

	PUSH	W10
	PUSH	W11
	PUSH	W12
	PUSH	W13
	ADD	W10, #16, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__DrawButton106
	GOTO	L_DrawButton4
L__DrawButton106:
	MOV	#lo_addr(_object_pressed), W0
	MOV.B	[W0], W0
	CP.B	W0, #1
	BRA Z	L__DrawButton107
	GOTO	L_DrawButton5
L__DrawButton107:
	MOV	#lo_addr(_object_pressed), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#34, W0
	ADD	W10, W0, W5
	MOV	#36, W0
	ADD	W10, W0, W4
	MOV	#32, W0
	ADD	W10, W0, W3
	ADD	W10, #31, W2
	MOV	#42, W0
	ADD	W10, W0, W1
	ADD	W10, #18, W0
	PUSH	W10
	ZE	[W3], W13
	ZE	[W2], W12
	MOV	[W1], W11
	ZE	[W0], W10
	PUSH	[W5]
	PUSH	[W4]
	CALL	_TFT_Set_Brush
	SUB	#4, W15
	POP	W10
	GOTO	L_DrawButton6
L_DrawButton5:
	MOV	#36, W0
	ADD	W10, W0, W5
	MOV	#34, W0
	ADD	W10, W0, W4
	MOV	#32, W0
	ADD	W10, W0, W3
	ADD	W10, #31, W2
	MOV	#38, W0
	ADD	W10, W0, W1
	ADD	W10, #18, W0
	PUSH	W10
	ZE	[W3], W13
	ZE	[W2], W12
	MOV	[W1], W11
	ZE	[W0], W10
	PUSH	[W5]
	PUSH	[W4]
	CALL	_TFT_Set_Brush
	SUB	#4, W15
	POP	W10
L_DrawButton6:
	ADD	W10, #12, W1
	ADD	W10, #14, W0
	PUSH	W10
	ZE	[W1], W11
	MOV	[W0], W10
	CALL	_TFT_Set_Pen
	POP	W10
	ADD	W10, #6, W3
	ADD	W10, #10, W0
	MOV	[W0], W0
	ADD	W0, [W3], W0
	SUB	W0, #1, W2
	ADD	W10, #4, W1
	ADD	W10, #8, W0
	MOV	[W0], W0
	ADD	W0, [W1], W0
	DEC	W0
	PUSH	W10
	MOV	W2, W13
	MOV	W0, W12
	MOV	[W3], W11
	MOV	[W1], W10
	CALL	_TFT_Rectangle
	POP	W10
	ADD	W10, #30, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__DrawButton108
	GOTO	L_DrawButton7
L__DrawButton108:
	ADD	W10, #28, W1
	ADD	W10, #24, W0
	PUSH	W10
	MOV	#2, W13
	MOV	[W1], W12
	MOV.D	[W0], W10
	CALL	_TFT_Set_Font
	POP	W10
	GOTO	L_DrawButton8
L_DrawButton7:
	ADD	W10, #28, W1
	ADD	W10, #24, W0
	PUSH	W10
	CLR	W13
	MOV	[W1], W12
	MOV.D	[W0], W10
	CALL	_TFT_Set_Font
	POP	W10
L_DrawButton8:
	ADD	W10, #6, W2
	ADD	W10, #4, W1
	ADD	W10, #20, W0
	PUSH	W10
	MOV	[W2], W12
	MOV	[W1], W11
	MOV	[W0], W10
	CALL	_TFT_Write_Text_Return_Pos
	POP	W10
	ADD	W10, #22, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA Z	L__DrawButton109
	GOTO	L_DrawButton9
L__DrawButton109:
	ADD	W10, #4, W0
	MOV	[W0], W1
	ADD	W14, #0, W0
	ADD	W1, #4, [W0]
	GOTO	L_DrawButton10
L_DrawButton9:
	ADD	W10, #22, W0
	MOV.B	[W0], W0
	CP.B	W0, #1
	BRA Z	L__DrawButton110
	GOTO	L_DrawButton11
L__DrawButton110:
	ADD	W10, #4, W2
	ADD	W10, #8, W0
	MOV	[W0], W1
	MOV	#lo_addr(_caption_length), W0
	SUB	W1, [W0], W0
	LSR	W0, #1, W1
	ADD	W14, #0, W0
	ADD	W1, [W2], [W0]
	GOTO	L_DrawButton12
L_DrawButton11:
	ADD	W10, #22, W0
	MOV.B	[W0], W0
	CP.B	W0, #2
	BRA Z	L__DrawButton111
	GOTO	L_DrawButton13
L__DrawButton111:
	ADD	W10, #4, W1
	ADD	W10, #8, W0
	MOV	[W0], W0
	ADD	W0, [W1], W1
	MOV	#lo_addr(_caption_length), W0
	SUB	W1, [W0], W1
	ADD	W14, #0, W0
	SUB	W1, #4, [W0]
L_DrawButton13:
L_DrawButton12:
L_DrawButton10:
	ADD	W10, #23, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA Z	L__DrawButton112
	GOTO	L_DrawButton14
L__DrawButton112:
	ADD	W10, #6, W0
	MOV	[W0], W1
	ADD	W14, #2, W0
	ADD	W1, #4, [W0]
	GOTO	L_DrawButton15
L_DrawButton14:
	ADD	W10, #23, W0
	MOV.B	[W0], W0
	CP.B	W0, #1
	BRA Z	L__DrawButton113
	GOTO	L_DrawButton16
L__DrawButton113:
	ADD	W10, #6, W2
	ADD	W10, #10, W0
	MOV	[W0], W1
	MOV	#lo_addr(_caption_height), W0
	SUB	W1, [W0], W0
	LSR	W0, #1, W1
	ADD	W14, #2, W0
	ADD	W1, [W2], [W0]
	GOTO	L_DrawButton17
L_DrawButton16:
	ADD	W10, #23, W0
	MOV.B	[W0], W0
	CP.B	W0, #2
	BRA Z	L__DrawButton114
	GOTO	L_DrawButton18
L__DrawButton114:
	ADD	W10, #6, W2
	ADD	W10, #10, W0
	MOV	[W0], W1
	MOV	#lo_addr(_caption_height), W0
	SUB	W1, [W0], W0
	SUB	W0, #4, W1
	ADD	W14, #2, W0
	ADD	W1, [W2], [W0]
L_DrawButton18:
L_DrawButton17:
L_DrawButton15:
	ADD	W10, #20, W0
	MOV	[W14+2], W12
	MOV	[W14+0], W11
	MOV	[W0], W10
	CALL	_TFT_Write_Text
L_DrawButton4:
L_end_DrawButton:
	POP	W13
	POP	W12
	POP	W11
	POP	W10
	ULNK
	RETURN
; end of _DrawButton

_DrawImage:

	PUSH	W10
	PUSH	W11
	PUSH	W12
	PUSH	W13
	ADD	W10, #16, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__DrawImage116
	GOTO	L_DrawImage19
L__DrawImage116:
	ADD	W10, #12, W2
	ADD	W10, #6, W1
	ADD	W10, #4, W0
	MOV.D	[W2], W12
	MOV	[W1], W11
	MOV	[W0], W10
	CALL	_TFT_Image_Jpeg
L_DrawImage19:
L_end_DrawImage:
	POP	W13
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _DrawImage

_DrawScreen:
	LNK	#12

	PUSH	W10
	PUSH	W11
	MOV	#lo_addr(_object_pressed), W1
	CLR	W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, [W14+0]
	CLR	W0
	MOV.B	W0, [W14+2]
	CLR	W0
	MOV.B	W0, [W14+6]
	MOV	W10, _CurrentScreen
	ADD	W10, #2, W0
	MOV	[W0], W1
	MOV	#lo_addr(_display_width), W0
	CP	W1, [W0]
	BRA Z	L__DrawScreen118
	GOTO	L__DrawScreen94
L__DrawScreen118:
	MOV	_CurrentScreen, W0
	ADD	W0, #4, W0
	MOV	[W0], W1
	MOV	#lo_addr(_display_height), W0
	CP	W1, [W0]
	BRA Z	L__DrawScreen119
	GOTO	L__DrawScreen93
L__DrawScreen119:
	GOTO	L_DrawScreen22
L__DrawScreen94:
L__DrawScreen93:
	ADD	W14, #10, W0
	CLR.B	[W0]
	BTSC	LATA7_bit, BitPos(LATA7_bit+0)
	INC.B	[W0], [W0]
	ADD	W14, #11, W0
	CLR.B	[W0]
	BTSC	TRISA7_bit, BitPos(TRISA7_bit+0)
	INC.B	[W0], [W0]
	BCLR	TRISA7_bit, BitPos(TRISA7_bit+0)
	BCLR	LATA7_bit, BitPos(LATA7_bit+0)
	MOV	_CurrentScreen, W0
	ADD	W0, #4, W1
	MOV	_CurrentScreen, W0
	INC2	W0
	MOV	[W1], W11
	MOV	[W0], W10
	CALL	_TFT_Init_SSD1963
	MOV	_CurrentScreen, W0
	ADD	W0, #4, W1
	MOV	_CurrentScreen, W0
	INC2	W0
	MOV	[W1], W11
	MOV	[W0], W10
	CALL	_STMPE610_SetSize
	MOV	_CurrentScreen, W0
	MOV	[W0], W10
	CALL	_TFT_Fill_Screen
	MOV	_CurrentScreen, W0
	INC2	W0
	MOV	[W0], W0
	MOV	W0, _display_width
	MOV	_CurrentScreen, W0
	ADD	W0, #4, W0
	MOV	[W0], W0
	MOV	W0, _display_height
	ADD	W14, #10, W0
	MOV.B	[W0], W0
	BTSS	W0, #0
	BCLR	LATA7_bit, BitPos(LATA7_bit+0)
	BTSC	W0, #0
	BSET	LATA7_bit, BitPos(LATA7_bit+0)
	ADD	W14, #11, W0
	MOV.B	[W0], W0
	BTSS	W0, #0
	BCLR	TRISA7_bit, BitPos(TRISA7_bit+0)
	BTSC	W0, #0
	BSET	TRISA7_bit, BitPos(TRISA7_bit+0)
	GOTO	L_DrawScreen23
L_DrawScreen22:
	MOV	_CurrentScreen, W0
	MOV	[W0], W10
	CALL	_TFT_Fill_Screen
L_DrawScreen23:
L_DrawScreen24:
	MOV	_CurrentScreen, W0
	ADD	W0, #6, W0
	MOV	[W0], W1
	ADD	W14, #0, W0
	CP	W1, [W0]
	BRA GTU	L__DrawScreen120
	GOTO	L_DrawScreen25
L__DrawScreen120:
	MOV	_CurrentScreen, W0
	ADD	W0, #8, W0
	MOV	[W0], W1
	ADD	W14, #2, W0
	ZE	[W0], W0
	CP	W0, W1
	BRA LTU	L__DrawScreen121
	GOTO	L_DrawScreen26
L__DrawScreen121:
	MOV	_CurrentScreen, W0
	ADD	W0, #10, W4
	ADD	W14, #2, W0
	ZE	[W0], W0
	CLR	W1
	SL	W0, W2
	RLC	W1, W3
	ADD	W2, [W4++], W0
	ADDC	W3, [W4--], W1
	MOV	W1, 50
	MOV	[W0], W0
	MOV	W0, [W14+4]
	INC2	W0
	MOV.B	[W0], W0
	ZE	W0, W1
	ADD	W14, #0, W0
	CP	W1, [W0]
	BRA Z	L__DrawScreen122
	GOTO	L_DrawScreen27
L__DrawScreen122:
	MOV.B	[W14+2], W1
	ADD	W14, #2, W0
	ADD.B	W1, #1, [W0]
	MOV	[W14+0], W1
	ADD	W14, #0, W0
	ADD	W1, #1, [W0]
	PUSH	W10
	MOV	[W14+4], W10
	CALL	_DrawButton
	POP	W10
L_DrawScreen27:
L_DrawScreen26:
	MOV	_CurrentScreen, W0
	ADD	W0, #14, W0
	MOV	[W0], W1
	ADD	W14, #6, W0
	ZE	[W0], W0
	CP	W0, W1
	BRA LTU	L__DrawScreen123
	GOTO	L_DrawScreen28
L__DrawScreen123:
	MOV	_CurrentScreen, W0
	ADD	W0, #16, W4
	ADD	W14, #6, W0
	ZE	[W0], W0
	CLR	W1
	SL	W0, W2
	RLC	W1, W3
	ADD	W2, [W4++], W0
	ADDC	W3, [W4--], W1
	MOV	W1, 50
	MOV	[W0], W0
	MOV	W0, [W14+8]
	INC2	W0
	MOV.B	[W0], W0
	ZE	W0, W1
	ADD	W14, #0, W0
	CP	W1, [W0]
	BRA Z	L__DrawScreen124
	GOTO	L_DrawScreen29
L__DrawScreen124:
	MOV.B	[W14+6], W1
	ADD	W14, #6, W0
	ADD.B	W1, #1, [W0]
	MOV	[W14+0], W1
	ADD	W14, #0, W0
	ADD	W1, #1, [W0]
	PUSH	W10
	MOV	[W14+8], W10
	CALL	_DrawImage
	POP	W10
L_DrawScreen29:
L_DrawScreen28:
	GOTO	L_DrawScreen24
L_DrawScreen25:
L_end_DrawScreen:
	POP	W11
	POP	W10
	ULNK
	RETURN
; end of _DrawScreen

_Get_Object:

	PUSH	W12
	PUSH	W13
	MOV	#65535, W0
	MOV	W0, _button_order
	MOV	#65535, W0
	MOV	W0, _image_order
	CLR	W0
	MOV	W0, __object_count
L_Get_Object30:
	MOV	_CurrentScreen, W0
	ADD	W0, #8, W0
	MOV	[W0], W1
	MOV	#lo_addr(__object_count), W0
	CP	W1, [W0]
	BRA GTU	L__Get_Object126
	GOTO	L_Get_Object31
L__Get_Object126:
	MOV	_CurrentScreen, W0
	ADD	W0, #10, W4
	MOV	__object_count, W0
	ASR	W0, #15, W1
	SL	W0, W2
	RLC	W1, W3
	ADD	W2, [W4++], W0
	ADDC	W3, [W4--], W1
	MOV	W1, 50
	MOV	[W0], W0
	MOV	W0, _local_button
	ADD	W0, #17, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__Get_Object127
	GOTO	L_Get_Object33
L__Get_Object127:
	MOV	_local_button, W0
	ADD	W0, #10, W3
	MOV	_local_button, W0
	ADD	W0, #8, W2
	MOV	_local_button, W0
	ADD	W0, #6, W1
	MOV	_local_button, W0
	ADD	W0, #4, W0
	MOV	[W1], W13
	MOV	[W0], W12
	PUSH	[W3]
	PUSH	[W2]
	CALL	MyProject_driver_IsInsideObject
	SUB	#4, W15
	CP.B	W0, #1
	BRA Z	L__Get_Object128
	GOTO	L_Get_Object34
L__Get_Object128:
	MOV	_local_button, W0
	INC2	W0
	MOV.B	[W0], W0
	ZE	W0, W0
	MOV	W0, _button_order
	MOV	_local_button, W0
	MOV	W0, _exec_button
L_Get_Object34:
L_Get_Object33:
	MOV	#1, W1
	MOV	#lo_addr(__object_count), W0
	ADD	W1, [W0], [W0]
	GOTO	L_Get_Object30
L_Get_Object31:
	CLR	W0
	MOV	W0, __object_count
L_Get_Object35:
	MOV	_CurrentScreen, W0
	ADD	W0, #14, W0
	MOV	[W0], W1
	MOV	#lo_addr(__object_count), W0
	CP	W1, [W0]
	BRA GTU	L__Get_Object129
	GOTO	L_Get_Object36
L__Get_Object129:
	MOV	_CurrentScreen, W0
	ADD	W0, #16, W4
	MOV	__object_count, W0
	ASR	W0, #15, W1
	SL	W0, W2
	RLC	W1, W3
	ADD	W2, [W4++], W0
	ADDC	W3, [W4--], W1
	MOV	W1, 50
	MOV	[W0], W0
	MOV	W0, _local_image
	ADD	W0, #17, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__Get_Object130
	GOTO	L_Get_Object38
L__Get_Object130:
	MOV	_local_image, W0
	ADD	W0, #10, W3
	MOV	_local_image, W0
	ADD	W0, #8, W2
	MOV	_local_image, W0
	ADD	W0, #6, W1
	MOV	_local_image, W0
	ADD	W0, #4, W0
	MOV	[W1], W13
	MOV	[W0], W12
	PUSH	[W3]
	PUSH	[W2]
	CALL	MyProject_driver_IsInsideObject
	SUB	#4, W15
	CP.B	W0, #1
	BRA Z	L__Get_Object131
	GOTO	L_Get_Object39
L__Get_Object131:
	MOV	_local_image, W0
	INC2	W0
	MOV.B	[W0], W0
	ZE	W0, W0
	MOV	W0, _image_order
	MOV	_local_image, W0
	MOV	W0, _exec_image
L_Get_Object39:
L_Get_Object38:
	MOV	#1, W1
	MOV	#lo_addr(__object_count), W0
	ADD	W1, [W0], [W0]
	GOTO	L_Get_Object35
L_Get_Object36:
	MOV	#65535, W0
	MOV	W0, __object_count
	MOV	_button_order, W1
	MOV	#65535, W0
	CP	W1, W0
	BRA GT	L__Get_Object132
	GOTO	L_Get_Object40
L__Get_Object132:
	MOV	_button_order, W0
	MOV	W0, __object_count
L_Get_Object40:
	MOV	_image_order, W1
	MOV	#lo_addr(__object_count), W0
	CP	W1, [W0]
	BRA GT	L__Get_Object133
	GOTO	L_Get_Object41
L__Get_Object133:
	MOV	_image_order, W0
	MOV	W0, __object_count
L_Get_Object41:
L_end_Get_Object:
	POP	W13
	POP	W12
	RETURN
; end of _Get_Object

_Process_TP_Press:

	CLR	W0
	MOV	W0, _exec_button
	CLR	W0
	MOV	W0, _exec_image
	CALL	_Get_Object
	MOV	#65535, W1
	MOV	#lo_addr(__object_count), W0
	CP	W1, [W0]
	BRA NZ	L__Process_TP_Press135
	GOTO	L_Process_TP_Press42
L__Process_TP_Press135:
	MOV	__object_count, W1
	MOV	#lo_addr(_button_order), W0
	CP	W1, [W0]
	BRA Z	L__Process_TP_Press136
	GOTO	L_Process_TP_Press43
L__Process_TP_Press136:
	MOV	_exec_button, W0
	ADD	W0, #17, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__Process_TP_Press137
	GOTO	L_Process_TP_Press44
L__Process_TP_Press137:
	MOV	#50, W1
	MOV	#lo_addr(_exec_button), W0
	ADD	W1, [W0], W0
	MOV	[W0], W0
	CP	W0, #0
	BRA NZ	L__Process_TP_Press138
	GOTO	L_Process_TP_Press45
L__Process_TP_Press138:
	MOV	#50, W1
	MOV	#lo_addr(_exec_button), W0
	ADD	W1, [W0], W0
	MOV	[W0], W0
	CALL	W0
	GOTO	L_end_Process_TP_Press
L_Process_TP_Press45:
L_Process_TP_Press44:
L_Process_TP_Press43:
	MOV	__object_count, W1
	MOV	#lo_addr(_image_order), W0
	CP	W1, [W0]
	BRA Z	L__Process_TP_Press139
	GOTO	L_Process_TP_Press46
L__Process_TP_Press139:
	MOV	_exec_image, W0
	ADD	W0, #17, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__Process_TP_Press140
	GOTO	L_Process_TP_Press47
L__Process_TP_Press140:
	MOV	_exec_image, W0
	ADD	W0, #26, W0
	MOV	[W0], W0
	CP	W0, #0
	BRA NZ	L__Process_TP_Press141
	GOTO	L_Process_TP_Press48
L__Process_TP_Press141:
	MOV	_exec_image, W0
	ADD	W0, #26, W0
	MOV	[W0], W0
	CALL	W0
	GOTO	L_end_Process_TP_Press
L_Process_TP_Press48:
L_Process_TP_Press47:
L_Process_TP_Press46:
L_Process_TP_Press42:
L_end_Process_TP_Press:
	RETURN
; end of _Process_TP_Press

_Process_TP_Up:

	GOTO	L_Process_TP_Up49
L_Process_TP_Up51:
	MOV	_PressedObject, W0
	CP	W0, #0
	BRA NZ	L__Process_TP_Up143
	GOTO	L_Process_TP_Up52
L__Process_TP_Up143:
	MOV	_PressedObject, W0
	MOV	W0, _exec_button
	MOV	#40, W1
	MOV	#lo_addr(_PressedObject), W0
	ADD	W1, [W0], W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__Process_TP_Up144
	GOTO	L__Process_TP_Up97
L__Process_TP_Up144:
	MOV	_exec_button, W0
	MOV	[W0], W1
	MOV	#lo_addr(_CurrentScreen), W0
	CP	W1, [W0]
	BRA Z	L__Process_TP_Up145
	GOTO	L__Process_TP_Up96
L__Process_TP_Up145:
L__Process_TP_Up95:
	PUSH.D	W10
	MOV	_exec_button, W10
	CALL	_DrawButton
	POP.D	W10
L__Process_TP_Up97:
L__Process_TP_Up96:
	GOTO	L_Process_TP_Up50
L_Process_TP_Up52:
	GOTO	L_Process_TP_Up50
L_Process_TP_Up49:
	MOV	_PressedObjectType, W0
	CP	W0, #0
	BRA NZ	L__Process_TP_Up146
	GOTO	L_Process_TP_Up51
L__Process_TP_Up146:
L_Process_TP_Up50:
	CLR	W0
	MOV	W0, _exec_image
	CALL	_Get_Object
	MOV	#65535, W1
	MOV	#lo_addr(__object_count), W0
	CP	W1, [W0]
	BRA NZ	L__Process_TP_Up147
	GOTO	L_Process_TP_Up56
L__Process_TP_Up147:
	MOV	__object_count, W1
	MOV	#lo_addr(_button_order), W0
	CP	W1, [W0]
	BRA Z	L__Process_TP_Up148
	GOTO	L_Process_TP_Up57
L__Process_TP_Up148:
	MOV	_exec_button, W0
	ADD	W0, #17, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__Process_TP_Up149
	GOTO	L_Process_TP_Up58
L__Process_TP_Up149:
	MOV	#44, W1
	MOV	#lo_addr(_exec_button), W0
	ADD	W1, [W0], W0
	MOV	[W0], W0
	CP	W0, #0
	BRA NZ	L__Process_TP_Up150
	GOTO	L_Process_TP_Up59
L__Process_TP_Up150:
	MOV	#44, W1
	MOV	#lo_addr(_exec_button), W0
	ADD	W1, [W0], W0
	MOV	[W0], W0
	CALL	W0
L_Process_TP_Up59:
	MOV	_PressedObject, W1
	MOV	#lo_addr(_exec_button), W0
	CP	W1, [W0]
	BRA Z	L__Process_TP_Up151
	GOTO	L_Process_TP_Up60
L__Process_TP_Up151:
	MOV	#48, W1
	MOV	#lo_addr(_exec_button), W0
	ADD	W1, [W0], W0
	MOV	[W0], W0
	CP	W0, #0
	BRA NZ	L__Process_TP_Up152
	GOTO	L_Process_TP_Up61
L__Process_TP_Up152:
	MOV	#48, W1
	MOV	#lo_addr(_exec_button), W0
	ADD	W1, [W0], W0
	MOV	[W0], W0
	CALL	W0
L_Process_TP_Up61:
L_Process_TP_Up60:
	CLR	W0
	MOV	W0, _PressedObject
	MOV	#65535, W0
	MOV	W0, _PressedObjectType
	GOTO	L_end_Process_TP_Up
L_Process_TP_Up58:
L_Process_TP_Up57:
	MOV	__object_count, W1
	MOV	#lo_addr(_image_order), W0
	CP	W1, [W0]
	BRA Z	L__Process_TP_Up153
	GOTO	L_Process_TP_Up62
L__Process_TP_Up153:
	MOV	_exec_image, W0
	ADD	W0, #17, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__Process_TP_Up154
	GOTO	L_Process_TP_Up63
L__Process_TP_Up154:
	MOV	_exec_image, W0
	ADD	W0, #20, W0
	MOV	[W0], W0
	CP	W0, #0
	BRA NZ	L__Process_TP_Up155
	GOTO	L_Process_TP_Up64
L__Process_TP_Up155:
	MOV	_exec_image, W0
	ADD	W0, #20, W0
	MOV	[W0], W0
	CALL	W0
L_Process_TP_Up64:
	MOV	_PressedObject, W1
	MOV	#lo_addr(_exec_image), W0
	CP	W1, [W0]
	BRA Z	L__Process_TP_Up156
	GOTO	L_Process_TP_Up65
L__Process_TP_Up156:
	MOV	_exec_image, W0
	ADD	W0, #24, W0
	MOV	[W0], W0
	CP	W0, #0
	BRA NZ	L__Process_TP_Up157
	GOTO	L_Process_TP_Up66
L__Process_TP_Up157:
	MOV	_exec_image, W0
	ADD	W0, #24, W0
	MOV	[W0], W0
	CALL	W0
L_Process_TP_Up66:
L_Process_TP_Up65:
	CLR	W0
	MOV	W0, _PressedObject
	MOV	#65535, W0
	MOV	W0, _PressedObjectType
	GOTO	L_end_Process_TP_Up
L_Process_TP_Up63:
L_Process_TP_Up62:
L_Process_TP_Up56:
	CLR	W0
	MOV	W0, _PressedObject
	MOV	#65535, W0
	MOV	W0, _PressedObjectType
L_end_Process_TP_Up:
	RETURN
; end of _Process_TP_Up

_Process_TP_Down:

	PUSH	W10
	MOV	#lo_addr(_object_pressed), W1
	CLR	W0
	MOV.B	W0, [W1]
	CLR	W0
	MOV	W0, _exec_button
	CLR	W0
	MOV	W0, _exec_image
	CALL	_Get_Object
	MOV	#65535, W1
	MOV	#lo_addr(__object_count), W0
	CP	W1, [W0]
	BRA NZ	L__Process_TP_Down159
	GOTO	L_Process_TP_Down67
L__Process_TP_Down159:
	MOV	__object_count, W1
	MOV	#lo_addr(_button_order), W0
	CP	W1, [W0]
	BRA Z	L__Process_TP_Down160
	GOTO	L_Process_TP_Down68
L__Process_TP_Down160:
	MOV	_exec_button, W0
	ADD	W0, #17, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__Process_TP_Down161
	GOTO	L_Process_TP_Down69
L__Process_TP_Down161:
	MOV	#40, W1
	MOV	#lo_addr(_exec_button), W0
	ADD	W1, [W0], W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__Process_TP_Down162
	GOTO	L_Process_TP_Down70
L__Process_TP_Down162:
	MOV	#lo_addr(_object_pressed), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	_exec_button, W10
	CALL	_DrawButton
L_Process_TP_Down70:
	MOV	_exec_button, W0
	MOV	W0, _PressedObject
	CLR	W0
	MOV	W0, _PressedObjectType
	MOV	#46, W1
	MOV	#lo_addr(_exec_button), W0
	ADD	W1, [W0], W0
	MOV	[W0], W0
	CP	W0, #0
	BRA NZ	L__Process_TP_Down163
	GOTO	L_Process_TP_Down71
L__Process_TP_Down163:
	MOV	#46, W1
	MOV	#lo_addr(_exec_button), W0
	ADD	W1, [W0], W0
	MOV	[W0], W0
	CALL	W0
	GOTO	L_end_Process_TP_Down
L_Process_TP_Down71:
L_Process_TP_Down69:
L_Process_TP_Down68:
	MOV	__object_count, W1
	MOV	#lo_addr(_image_order), W0
	CP	W1, [W0]
	BRA Z	L__Process_TP_Down164
	GOTO	L_Process_TP_Down72
L__Process_TP_Down164:
	MOV	_exec_image, W0
	ADD	W0, #17, W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA NZ	L__Process_TP_Down165
	GOTO	L_Process_TP_Down73
L__Process_TP_Down165:
	MOV	_exec_image, W0
	MOV	W0, _PressedObject
	MOV	#3, W0
	MOV	W0, _PressedObjectType
	MOV	_exec_image, W0
	ADD	W0, #22, W0
	MOV	[W0], W0
	CP	W0, #0
	BRA NZ	L__Process_TP_Down166
	GOTO	L_Process_TP_Down74
L__Process_TP_Down166:
	MOV	_exec_image, W0
	ADD	W0, #22, W0
	MOV	[W0], W0
	CALL	W0
	GOTO	L_end_Process_TP_Down
L_Process_TP_Down74:
L_Process_TP_Down73:
L_Process_TP_Down72:
L_Process_TP_Down67:
L_end_Process_TP_Down:
	POP	W10
	RETURN
; end of _Process_TP_Down

_Check_TP:

	PUSH	W10
	PUSH	W11
	CALL	_STMPE610_PressDetect
	CP0.B	W0
	BRA NZ	L__Check_TP168
	GOTO	L_Check_TP75
L__Check_TP168:
	MOV	#lo_addr(_Ycoord), W11
	MOV	#lo_addr(_Xcoord), W10
	CALL	_STMPE610_GetLastCoordinates
	CP.B	W0, #0
	BRA Z	L__Check_TP169
	GOTO	L_Check_TP76
L__Check_TP169:
	MOV	_Ycoord, W11
	MOV	_Xcoord, W10
	CALL	_Process_TP_Press
	MOV	#lo_addr(_PenDown), W0
	MOV.B	[W0], W0
	CP.B	W0, #0
	BRA Z	L__Check_TP170
	GOTO	L_Check_TP77
L__Check_TP170:
	MOV	#lo_addr(_PenDown), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	_Ycoord, W11
	MOV	_Xcoord, W10
	CALL	_Process_TP_Down
L_Check_TP77:
L_Check_TP76:
	GOTO	L_Check_TP78
L_Check_TP75:
	MOV	#lo_addr(_PenDown), W0
	MOV.B	[W0], W0
	CP.B	W0, #1
	BRA Z	L__Check_TP171
	GOTO	L_Check_TP79
L__Check_TP171:
	MOV	#lo_addr(_PenDown), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	_Ycoord, W11
	MOV	_Xcoord, W10
	CALL	_Process_TP_Up
L_Check_TP79:
L_Check_TP78:
L_end_Check_TP:
	POP	W11
	POP	W10
	RETURN
; end of _Check_TP

_Init_MCU:

	MOV	#13280, W0
	MOV	WREG, OSCCON
	MOV	#6, W0
	MOV	WREG, CLKDIV
	MOV	#278, W0
	MOV	WREG, PLLFBD
	CLR	ANSELD
	CLR	ANSELA
L_end_Init_MCU:
	RETURN
; end of _Init_MCU

_STMPE610_Config:

	PUSH	W10
	PUSH	W11
	PUSH	W12
	MOV	#34464, W10
	MOV	#1, W11
	CALL	_I2C2_Init
	MOV.B	#136, W10
	CALL	_STMPE610_SetI2CAddress
	CALL	_STMPE610_IsOperational
	CP.B	W0, #0
	BRA NZ	L__STMPE610_Config174
	GOTO	L_STMPE610_Config80
L__STMPE610_Config174:
	MOV.B	#4, W0
	GOTO	L_end_STMPE610_Config
L_STMPE610_Config80:
	CALL	_STMPE610_Reset
	MOV.B	#1, W11
	MOV.B	#3, W10
	CALL	_STMPE610_Module
	MOV.B	#1, W11
	MOV.B	#2, W10
	CALL	_STMPE610_AlternateFunction
	CLR	W11
	MOV.B	#2, W10
	CALL	_STMPE610_SetGPIOPin
	MOV	#272, W11
	MOV	#480, W10
	CALL	_STMPE610_SetSize
	MOV.B	#1, W11
	MOV.B	#3, W10
	CALL	_STMPE610_Module
	CLR	W10
	CALL	_STMPE610_ConfigureInterrupt
	MOV.B	#20, W10
	CALL	_STMPE610_SetADC
	CALL	_Delay_10ms
	CALL	_Delay_10ms
	MOV.B	#1, W10
	CALL	_STMPE610_SetADCClock
	CLR	W11
	MOV.B	#240, W10
	CALL	_STMPE610_AlternateFunction
	MOV.B	#2, W12
	MOV.B	#24, W11
	MOV.B	#128, W10
	CALL	_STMPE610_ConfigureTSC
	MOV.B	#1, W10
	CALL	_STMPE610_SetFIFOThreshold
	CALL	_STMPE610_ResetFIFO
	CLR	W10
	CALL	_STMPE610_TSIDrive
	MOV.B	#1, W10
	CALL	_STMPE610_TSControl
	MOV.B	#4, W10
	CALL	_STMPE610_ZDataFraction
	MOV.B	#70, W10
	CALL	_STMPE610_SetTouchPressureThreshold
	CALL	_STMPE610_ClearInterrupts
	MOV.B	#1, W11
	MOV.B	#9, W10
	CALL	_STMPE610_WriteReg
	CLR	W0
L_end_STMPE610_Config:
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _STMPE610_Config

_Start_TP:
	LNK	#10

	PUSH	W10
	CALL	_Init_MCU
	CALL	MyProject_driver_InitializeTouchPanel
	CALL	_STMPE610_Config
	CALL	_STMPE610_Config
	CP.B	W0, #0
	BRA Z	L__Start_TP176
	GOTO	L_Start_TP81
L__Start_TP176:
	GOTO	L_Start_TP82
L_Start_TP81:
L_Start_TP83:
	GOTO	L_Start_TP83
L_Start_TP82:
	MOV	#105, W0
	MOV	W0, [W14+0]
	MOV	#3925, W0
	MOV	W0, [W14+2]
	MOV	#250, W0
	MOV	W0, [W14+4]
	MOV	#3820, W0
	MOV	W0, [W14+6]
	CLR	W0
	MOV.B	W0, [W14+8]
	ADD	W14, #0, W0
	MOV	W0, W10
	CALL	_STMPE610_SetCalibrationConsts
	CALL	MyProject_driver_InitializeObjects
	MOV	_Screen1+2, W0
	MOV	W0, _display_width
	MOV	_Screen1+4, W0
	MOV	W0, _display_height
	MOV	#lo_addr(_Screen1), W10
	CALL	_DrawScreen
	MOV.B	#255, W10
	CALL	_TFT_Set_DBC_SSD1963
	MOV	#428, W8
	MOV	#16384, W7
L_Start_TP85:
	DEC	W7
	BRA NZ	L_Start_TP85
	DEC	W8
	BRA NZ	L_Start_TP85
L_end_Start_TP:
	POP	W10
	ULNK
	RETURN
; end of _Start_TP

MyProject_driver____?ag:

L_end_MyProject_driver___?ag:
	RETURN
; end of MyProject_driver____?ag
