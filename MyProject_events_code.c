#include "MyProject_objects.h"
#include "MyProject_resources.h"

//--------------------- User code ---------------------//
//-----------------------W/R Control bytes--------------------------------------
#define Chip1W      0x40  //0b0100 + 0b000(Slave address) + 0b0(Write) => 0x40


//----------------- End of User code ------------------//

// Event Handlers
unsigned short testvar = 0x01;
extern void EXPANDER_write(char chipxx, char portAddr, char dat);
static unsigned short maskstart = 0x03;
static unsigned short maskjog = 0x04;
static unsigned short maskcrawl = 0x08;
static unsigned short maskspeedup = 0x10;
static unsigned short maskspeedown = 0x20;
static unsigned short maskcoast = 0x40;
//start function
void Button1OnClick() {
     testvar ^= maskstart;
     //testvar |= maskcoast;
     // activate first port on Digital output
     EXPANDER_write(Chip1W, 0x14,testvar);
     if ((testvar & maskstart) == 0x01)
     {
     Button1.Gradient_Start_Color = 0xF800;
     Button1.Caption = "Stop";
     DrawButton(&Button1);
     Button2.Active = 0;
     Button3.Active = 0;
     }
     else
     {
     Button1.Gradient_Start_Color = 0x67E0;
     Button1.Caption = "Start";
     DrawButton(&Button1);
     Button6.Gradient_Start_Color = 0x6013;
     DrawButton(&Button6);
     Button2.Active = 1;
     Button3.Active = 1;
     }
}
//jog function
void Button2Onclick() {
     testvar ^= maskjog;
     //testvar |= maskcoast;
     EXPANDER_write(Chip1W, 0x14,testvar);
     if ((testvar & maskjog) == maskjog)
     {
     Button2.Gradient_Start_Color = 0xF800;
     Button2.Caption = "Stop Jog";
     DrawButton(&Button2);
     Button6.Gradient_Start_Color = 0x6013;
     DrawButton(&Button6);
     Button4.Active = 0;
     Button5.Active = 0;
     }
     else if ((testvar & maskjog) != maskjog)
     {
     Button2.Gradient_Start_Color = 0xCE66;
     Button2.Caption = "Jog";
     DrawButton(&Button2);
     Button6.Gradient_Start_Color = 0x6013;
     DrawButton(&Button6);
     Button4.Active = 1;
     Button5.Active = 1;
     }
}
//Button3 is for Crawl
void Button3Onpush() {
     testvar ^= maskcrawl;
     //testvar |= maskcoast;
     EXPANDER_write(Chip1W, 0x14,testvar);
     Button3.Gradient_Start_Color = 0x300C;
     DrawButton(&Button3);
     Button6.Gradient_Start_Color = 0x6013;
     DrawButton(&Button6);
}
void Button3Onrelease() {
     testvar ^= maskcrawl;
    // testvar |= maskcoast;
     EXPANDER_write(Chip1W, 0x14,testvar);
     Button3.Gradient_Start_Color = 0xC993;
     DrawButton(&Button3);
     Button6.Gradient_Start_Color = 0x6013;
     DrawButton(&Button6);
}
// Button4 is for Speed Up function
void Button4Onpush() {
     testvar ^= maskspeedup;
     //testvar |= maskcoast;
     EXPANDER_write(Chip1W, 0x14,testvar);
     Button4.Gradient_Start_Color = 0x3320;
     DrawButton(&Button4);
     Button6.Gradient_Start_Color = 0x6013;
     DrawButton(&Button6);
}
void Button4Onrelease() {
     testvar ^= maskspeedup;
     //testvar |= maskcoast;
     EXPANDER_write(Chip1W, 0x14,testvar);
     Button4.Gradient_Start_Color = 0x3660;
     DrawButton(&Button4);
     Button6.Gradient_Start_Color = 0x6013;
     DrawButton(&Button6);
}
//Button5 is for Speed Down function
void Button5Onpush() {
     testvar ^= maskspeedown;
     //testvar |= maskcoast;
     EXPANDER_write(Chip1W, 0x14,testvar);
     Button5.Gradient_Start_Color = 0xF800;
     DrawButton(&Button5);
     Button6.Gradient_Start_Color = 0x6013;
     DrawButton(&Button6);
}
void Button5Onrelease() {
     testvar ^= maskspeedown;
     //testvar |= maskcoast;
     EXPANDER_write(Chip1W, 0x14,testvar);
     Button5.Gradient_Start_Color = 0xC980;
     DrawButton(&Button5);
     Button6.Gradient_Start_Color = 0x6013;
     DrawButton(&Button6);
}
//jump to homepage
void Image1Onclick() {
    DrawScreen(&Screen2);
}
//Coast (emergency) function
void Button6Onclick() {
     testvar = maskcoast;
     EXPANDER_write(Chip1W, 0x14,testvar);
     testvar = 0x01;
     Button6.Gradient_Start_Color = 0xF800;
     DrawButton(&Button6);
     Button1.Gradient_Start_Color = 0x67E0;
     Button1.Caption = "Start";
     DrawButton(&Button1);
     Button2.Gradient_Start_Color = 0xCE66;
     Button2.Caption = "Jog";
     DrawButton(&Button2);
}