
_Button1OnClick:

	PUSH	W10
	PUSH	W11
	PUSH	W12
	MOV	#lo_addr(MyProject_events_code_maskstart), W0
	MOV.B	[W0], W1
	MOV	#lo_addr(_testvar), W0
	XOR.B	W1, [W0], [W0]
	MOV	#lo_addr(_testvar), W0
	MOV.B	[W0], W12
	MOV.B	#20, W11
	MOV.B	#64, W10
	CALL	_EXPANDER_write
	MOV	#lo_addr(_testvar), W0
	ZE	[W0], W1
	MOV	#lo_addr(MyProject_events_code_maskstart), W0
	ZE	[W0], W0
	AND	W1, W0, W0
	CP	W0, #1
	BRA Z	L__Button1OnClick6
	GOTO	L_Button1OnClick0
L__Button1OnClick6:
	MOV	#63488, W0
	MOV	W0, _Button1+34
	MOV	#lo_addr(?lstr1_MyProject_events_code), W0
	MOV	W0, _Button1+20
	MOV	#lo_addr(_Button1), W10
	CALL	_DrawButton
	MOV	#lo_addr(_Button2+17), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button3+17), W1
	CLR	W0
	MOV.B	W0, [W1]
	GOTO	L_Button1OnClick1
L_Button1OnClick0:
	MOV	#26592, W0
	MOV	W0, _Button1+34
	MOV	#lo_addr(?lstr2_MyProject_events_code), W0
	MOV	W0, _Button1+20
	MOV	#lo_addr(_Button1), W10
	CALL	_DrawButton
	MOV	#24595, W0
	MOV	W0, _Button6+34
	MOV	#lo_addr(_Button6), W10
	CALL	_DrawButton
	MOV	#lo_addr(_Button2+17), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button3+17), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
L_Button1OnClick1:
L_end_Button1OnClick:
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _Button1OnClick

_Button2Onclick:

	PUSH	W10
	PUSH	W11
	PUSH	W12
	MOV	#lo_addr(MyProject_events_code_maskjog), W0
	MOV.B	[W0], W1
	MOV	#lo_addr(_testvar), W0
	XOR.B	W1, [W0], [W0]
	MOV	#lo_addr(_testvar), W0
	MOV.B	[W0], W12
	MOV.B	#20, W11
	MOV.B	#64, W10
	CALL	_EXPANDER_write
	MOV	#lo_addr(_testvar), W0
	ZE	[W0], W1
	MOV	#lo_addr(MyProject_events_code_maskjog), W0
	ZE	[W0], W0
	AND	W1, W0, W1
	MOV	#lo_addr(MyProject_events_code_maskjog), W0
	ZE	[W0], W0
	CP	W1, W0
	BRA Z	L__Button2Onclick8
	GOTO	L_Button2Onclick2
L__Button2Onclick8:
	MOV	#63488, W0
	MOV	W0, _Button2+34
	MOV	#lo_addr(?lstr3_MyProject_events_code), W0
	MOV	W0, _Button2+20
	MOV	#lo_addr(_Button2), W10
	CALL	_DrawButton
	MOV	#24595, W0
	MOV	W0, _Button6+34
	MOV	#lo_addr(_Button6), W10
	CALL	_DrawButton
	MOV	#lo_addr(_Button4+17), W1
	CLR	W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button5+17), W1
	CLR	W0
	MOV.B	W0, [W1]
	GOTO	L_Button2Onclick3
L_Button2Onclick2:
	MOV	#lo_addr(_testvar), W0
	ZE	[W0], W1
	MOV	#lo_addr(MyProject_events_code_maskjog), W0
	ZE	[W0], W0
	AND	W1, W0, W1
	MOV	#lo_addr(MyProject_events_code_maskjog), W0
	ZE	[W0], W0
	CP	W1, W0
	BRA NZ	L__Button2Onclick9
	GOTO	L_Button2Onclick4
L__Button2Onclick9:
	MOV	#52838, W0
	MOV	W0, _Button2+34
	MOV	#lo_addr(?lstr4_MyProject_events_code), W0
	MOV	W0, _Button2+20
	MOV	#lo_addr(_Button2), W10
	CALL	_DrawButton
	MOV	#24595, W0
	MOV	W0, _Button6+34
	MOV	#lo_addr(_Button6), W10
	CALL	_DrawButton
	MOV	#lo_addr(_Button4+17), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#lo_addr(_Button5+17), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
L_Button2Onclick4:
L_Button2Onclick3:
L_end_Button2Onclick:
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _Button2Onclick

_Button3Onpush:

	PUSH	W10
	PUSH	W11
	PUSH	W12
	MOV	#lo_addr(MyProject_events_code_maskcrawl), W0
	MOV.B	[W0], W1
	MOV	#lo_addr(_testvar), W0
	XOR.B	W1, [W0], [W0]
	MOV	#lo_addr(_testvar), W0
	MOV.B	[W0], W12
	MOV.B	#20, W11
	MOV.B	#64, W10
	CALL	_EXPANDER_write
	MOV	#12300, W0
	MOV	W0, _Button3+34
	MOV	#lo_addr(_Button3), W10
	CALL	_DrawButton
	MOV	#24595, W0
	MOV	W0, _Button6+34
	MOV	#lo_addr(_Button6), W10
	CALL	_DrawButton
L_end_Button3Onpush:
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _Button3Onpush

_Button3Onrelease:

	PUSH	W10
	PUSH	W11
	PUSH	W12
	MOV	#lo_addr(MyProject_events_code_maskcrawl), W0
	MOV.B	[W0], W1
	MOV	#lo_addr(_testvar), W0
	XOR.B	W1, [W0], [W0]
	MOV	#lo_addr(_testvar), W0
	MOV.B	[W0], W12
	MOV.B	#20, W11
	MOV.B	#64, W10
	CALL	_EXPANDER_write
	MOV	#51603, W0
	MOV	W0, _Button3+34
	MOV	#lo_addr(_Button3), W10
	CALL	_DrawButton
	MOV	#24595, W0
	MOV	W0, _Button6+34
	MOV	#lo_addr(_Button6), W10
	CALL	_DrawButton
L_end_Button3Onrelease:
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _Button3Onrelease

_Button4Onpush:

	PUSH	W10
	PUSH	W11
	PUSH	W12
	MOV	#lo_addr(MyProject_events_code_maskspeedup), W0
	MOV.B	[W0], W1
	MOV	#lo_addr(_testvar), W0
	XOR.B	W1, [W0], [W0]
	MOV	#lo_addr(_testvar), W0
	MOV.B	[W0], W12
	MOV.B	#20, W11
	MOV.B	#64, W10
	CALL	_EXPANDER_write
	MOV	#13088, W0
	MOV	W0, _Button4+34
	MOV	#lo_addr(_Button4), W10
	CALL	_DrawButton
	MOV	#24595, W0
	MOV	W0, _Button6+34
	MOV	#lo_addr(_Button6), W10
	CALL	_DrawButton
L_end_Button4Onpush:
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _Button4Onpush

_Button4Onrelease:

	PUSH	W10
	PUSH	W11
	PUSH	W12
	MOV	#lo_addr(MyProject_events_code_maskspeedup), W0
	MOV.B	[W0], W1
	MOV	#lo_addr(_testvar), W0
	XOR.B	W1, [W0], [W0]
	MOV	#lo_addr(_testvar), W0
	MOV.B	[W0], W12
	MOV.B	#20, W11
	MOV.B	#64, W10
	CALL	_EXPANDER_write
	MOV	#13920, W0
	MOV	W0, _Button4+34
	MOV	#lo_addr(_Button4), W10
	CALL	_DrawButton
	MOV	#24595, W0
	MOV	W0, _Button6+34
	MOV	#lo_addr(_Button6), W10
	CALL	_DrawButton
L_end_Button4Onrelease:
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _Button4Onrelease

_Button5Onpush:

	PUSH	W10
	PUSH	W11
	PUSH	W12
	MOV	#lo_addr(MyProject_events_code_maskspeedown), W0
	MOV.B	[W0], W1
	MOV	#lo_addr(_testvar), W0
	XOR.B	W1, [W0], [W0]
	MOV	#lo_addr(_testvar), W0
	MOV.B	[W0], W12
	MOV.B	#20, W11
	MOV.B	#64, W10
	CALL	_EXPANDER_write
	MOV	#63488, W0
	MOV	W0, _Button5+34
	MOV	#lo_addr(_Button5), W10
	CALL	_DrawButton
	MOV	#24595, W0
	MOV	W0, _Button6+34
	MOV	#lo_addr(_Button6), W10
	CALL	_DrawButton
L_end_Button5Onpush:
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _Button5Onpush

_Button5Onrelease:

	PUSH	W10
	PUSH	W11
	PUSH	W12
	MOV	#lo_addr(MyProject_events_code_maskspeedown), W0
	MOV.B	[W0], W1
	MOV	#lo_addr(_testvar), W0
	XOR.B	W1, [W0], [W0]
	MOV	#lo_addr(_testvar), W0
	MOV.B	[W0], W12
	MOV.B	#20, W11
	MOV.B	#64, W10
	CALL	_EXPANDER_write
	MOV	#51584, W0
	MOV	W0, _Button5+34
	MOV	#lo_addr(_Button5), W10
	CALL	_DrawButton
	MOV	#24595, W0
	MOV	W0, _Button6+34
	MOV	#lo_addr(_Button6), W10
	CALL	_DrawButton
L_end_Button5Onrelease:
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _Button5Onrelease

_Image1Onclick:

	PUSH	W10
	MOV	#lo_addr(_Screen2), W10
	CALL	_DrawScreen
L_end_Image1Onclick:
	POP	W10
	RETURN
; end of _Image1Onclick

_Button6Onclick:

	PUSH	W10
	PUSH	W11
	PUSH	W12
	MOV	#lo_addr(_testvar), W1
	MOV	#lo_addr(MyProject_events_code_maskcoast), W0
	MOV.B	[W0], [W1]
	MOV	#lo_addr(MyProject_events_code_maskcoast), W0
	MOV.B	[W0], W12
	MOV.B	#20, W11
	MOV.B	#64, W10
	CALL	_EXPANDER_write
	MOV	#lo_addr(_testvar), W1
	MOV.B	#1, W0
	MOV.B	W0, [W1]
	MOV	#63488, W0
	MOV	W0, _Button6+34
	MOV	#lo_addr(_Button6), W10
	CALL	_DrawButton
	MOV	#26592, W0
	MOV	W0, _Button1+34
	MOV	#lo_addr(?lstr5_MyProject_events_code), W0
	MOV	W0, _Button1+20
	MOV	#lo_addr(_Button1), W10
	CALL	_DrawButton
	MOV	#52838, W0
	MOV	W0, _Button2+34
	MOV	#lo_addr(?lstr6_MyProject_events_code), W0
	MOV	W0, _Button2+20
	MOV	#lo_addr(_Button2), W10
	CALL	_DrawButton
L_end_Button6Onclick:
	POP	W12
	POP	W11
	POP	W10
	RETURN
; end of _Button6Onclick
