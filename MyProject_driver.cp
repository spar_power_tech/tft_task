#line 1 "C:/Users/James/Desktop/SPARR/SPAR/Program/Demo_touch_back_up - Copy/mikroC/MyProject_driver.c"
#line 1 "c:/users/james/desktop/sparr/spar/program/demo_touch_back_up - copy/mikroc/myproject_objects.h"
typedef enum {_taLeft, _taCenter, _taRight} TTextAlign;
typedef enum {_tavTop, _tavMiddle, _tavBottom} TTextAlignVertical;

typedef struct Screen TScreen;

typedef struct {
 unsigned int X_Min;
 unsigned int X_Max;
 unsigned int Y_Min;
 unsigned int Y_Max;
 char Rotate;
} TTPConstants;

typedef struct Button {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 char *Caption;
 TTextAlign TextAlign;
 TTextAlignVertical TextAlignVertical;
 const far char *FontName;
 unsigned int Font_Color;
 char VerticalText;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TButton;

typedef struct Image {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 const far char *Picture_Name;
 char Visible;
 char Active;
 char Picture_Type;
 char Picture_Ratio;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TImage;

struct Screen {
 unsigned int Color;
 unsigned int Width;
 unsigned int Height;
 unsigned int ObjectsCount;
 unsigned int ButtonsCount;
 TButton * const code far *Buttons;
 unsigned int ImagesCount;
 TImage * const code far *Images;
};

extern TScreen Screen1;
extern TImage Image1;
extern TImage * const code far Screen1_Images[1];

extern TScreen Screen2;
extern TButton Button1;
extern TButton Button2;
extern TButton Button3;
extern TButton Button4;
extern TButton Button5;
extern TButton Button6;
extern TButton * const code far Screen2_Buttons[6];




void Button1OnClick();
void Button2Onclick();
void Button3Onpush();
void Button3Onrelease();
void Button4Onpush();
void Button4Onrelease();
void Button5Onpush();
void Button5Onrelease();
void Button6Onclick();
void Image1Onclick();




extern char Image1_Caption[];
extern char Button1_Caption[];
extern char Button2_Caption[];
extern char Button3_Caption[];
extern char Button4_Caption[];
extern char Button5_Caption[];
extern char Button6_Caption[];


void DrawScreen(TScreen *aScreen);
void DrawButton(TButton *aButton);
void DrawImage(TImage *AImage);
void Check_TP();
void Start_TP();
void Process_TP_Press(unsigned int X, unsigned int Y);
void Process_TP_Up(unsigned int X, unsigned int Y);
void Process_TP_Down(unsigned int X, unsigned int Y);
#line 1 "c:/users/james/desktop/sparr/spar/program/demo_touch_back_up - copy/mikroc/myproject_resources.h"
const code far char Tahoma11x13_Regular[];
const code far char all_jpg[13852];
#line 1 "c:/users/public/documents/mikroelektronika/mikroc pro for dspic/include/built_in.h"
#line 7 "C:/Users/James/Desktop/SPARR/SPAR/Program/Demo_touch_back_up - Copy/mikroC/MyProject_driver.c"
unsigned int TFT_DATAPORT at LATD;
sbit TFT_RST at LATA0_bit;
sbit TFT_BLED at LATA7_bit;
sbit TFT_RS at LATA4_bit;
sbit TFT_CS at LATA1_bit;
sbit TFT_RD at LATA5_bit;
sbit TFT_WR at LATA6_bit;
unsigned int TFT_DATAPORT_DIRECTION at TRISD;
sbit TFT_RST_DIRECTION at TRISA0_bit;
sbit TFT_BLED_DIRECTION at TRISA7_bit;
sbit TFT_RS_DIRECTION at TRISA4_bit;
sbit TFT_CS_DIRECTION at TRISA1_bit;
sbit TFT_RD_DIRECTION at TRISA5_bit;
sbit TFT_WR_DIRECTION at TRISA6_bit;




unsigned int Xcoord, Ycoord;
char PenDown;
void *PressedObject;
int PressedObjectType;
unsigned int caption_length, caption_height;
unsigned int display_width, display_height;

int _object_count;
unsigned short object_pressed;
TButton *local_button;
TButton *exec_button;
int button_order;
TImage *local_image;
TImage *exec_image;
int image_order;

static void InitializeTouchPanel() {
 TFT_DataPort_Direction = 0;
 TFT_Init_SSD1963(480, 272);


 PenDown = 0;
 PressedObject = 0;
 PressedObjectType = -1;
}



 TScreen* CurrentScreen;

 TScreen Screen1;
 TImage Image1;
 TImage * const code far Screen1_Images[1]=
 {
 &Image1
 };

 TScreen Screen2;
 TButton Button1;
char Button1_Caption[6] = "Start";

 TButton Button2;
char Button2_Caption[4] = "Jog";

 TButton Button3;
char Button3_Caption[6] = "Crawl";

 TButton Button4;
char Button4_Caption[9] = "Speed Up";

 TButton Button5;
char Button5_Caption[11] = "Speed Down";

 TButton Button6;
char Button6_Caption[6] = "Coast";

 TButton * const code far Screen2_Buttons[6]=
 {
 &Button1,
 &Button2,
 &Button3,
 &Button4,
 &Button5,
 &Button6
 };



static void InitializeObjects() {
 Screen1.Color = 0x5AEB;
 Screen1.Width = 480;
 Screen1.Height = 272;
 Screen1.ButtonsCount = 0;
 Screen1.ImagesCount = 1;
 Screen1.Images = Screen1_Images;
 Screen1.ObjectsCount = 1;

 Screen2.Color = 0x0208;
 Screen2.Width = 480;
 Screen2.Height = 272;
 Screen2.ButtonsCount = 6;
 Screen2.Buttons = Screen2_Buttons;
 Screen2.ImagesCount = 0;
 Screen2.ObjectsCount = 6;


 Image1.OwnerScreen = &Screen1;
 Image1.Order = 0;
 Image1.Left = 0;
 Image1.Top = 0;
 Image1.Width = 480;
 Image1.Height = 272;
 Image1.Picture_Type = 1;
 Image1.Picture_Ratio = 1;
 Image1.Picture_Name = all_jpg;
 Image1.Visible = 1;
 Image1.Active = 1;
 Image1.OnUpPtr = 0;
 Image1.OnDownPtr = 0;
 Image1.OnClickPtr = Image1Onclick;
 Image1.OnPressPtr = 0;

 Button1.OwnerScreen = &Screen2;
 Button1.Order = 0;
 Button1.Left = 43;
 Button1.Top = 33;
 Button1.Width = 83;
 Button1.Height = 72;
 Button1.Pen_Width = 1;
 Button1.Pen_Color = 0x0000;
 Button1.Visible = 1;
 Button1.Active = 1;
 Button1.Transparent = 1;
 Button1.Caption = Button1_Caption;
 Button1.TextAlign = _taCenter;
 Button1.TextAlignVertical= _tavMiddle;
 Button1.FontName = Tahoma11x13_Regular;
 Button1.PressColEnabled = 1;
 Button1.Font_Color = 0x0000;
 Button1.VerticalText = 0;
 Button1.Gradient = 1;
 Button1.Gradient_Orientation = 0;
 Button1.Gradient_Start_Color = 0x67E0;
 Button1.Gradient_End_Color = 0xFFFF;
 Button1.Color = 0xC618;
 Button1.Press_Color = 0x3019;
 Button1.OnUpPtr = 0;
 Button1.OnDownPtr = 0;
 Button1.OnClickPtr = Button1OnClick;
 Button1.OnPressPtr = 0;

 Button2.OwnerScreen = &Screen2;
 Button2.Order = 1;
 Button2.Left = 196;
 Button2.Top = 33;
 Button2.Width = 83;
 Button2.Height = 72;
 Button2.Pen_Width = 1;
 Button2.Pen_Color = 0x0000;
 Button2.Visible = 1;
 Button2.Active = 1;
 Button2.Transparent = 1;
 Button2.Caption = Button2_Caption;
 Button2.TextAlign = _taCenter;
 Button2.TextAlignVertical= _tavMiddle;
 Button2.FontName = Tahoma11x13_Regular;
 Button2.PressColEnabled = 1;
 Button2.Font_Color = 0x0000;
 Button2.VerticalText = 0;
 Button2.Gradient = 1;
 Button2.Gradient_Orientation = 0;
 Button2.Gradient_Start_Color = 0xCE66;
 Button2.Gradient_End_Color = 0xC618;
 Button2.Color = 0xC618;
 Button2.Press_Color = 0x001F;
 Button2.OnUpPtr = 0;
 Button2.OnDownPtr = 0;
 Button2.OnClickPtr = Button2Onclick;
 Button2.OnPressPtr = 0;

 Button3.OwnerScreen = &Screen2;
 Button3.Order = 2;
 Button3.Left = 349;
 Button3.Top = 33;
 Button3.Width = 83;
 Button3.Height = 72;
 Button3.Pen_Width = 1;
 Button3.Pen_Color = 0x0000;
 Button3.Visible = 1;
 Button3.Active = 1;
 Button3.Transparent = 1;
 Button3.Caption = Button3_Caption;
 Button3.TextAlign = _taCenter;
 Button3.TextAlignVertical= _tavMiddle;
 Button3.FontName = Tahoma11x13_Regular;
 Button3.PressColEnabled = 1;
 Button3.Font_Color = 0x0000;
 Button3.VerticalText = 0;
 Button3.Gradient = 1;
 Button3.Gradient_Orientation = 0;
 Button3.Gradient_Start_Color = 0xC993;
 Button3.Gradient_End_Color = 0xC618;
 Button3.Color = 0xC618;
 Button3.Press_Color = 0x001F;
 Button3.OnUpPtr = Button3Onrelease;
 Button3.OnDownPtr = Button3Onpush;
 Button3.OnClickPtr = 0;
 Button3.OnPressPtr = 0;

 Button4.OwnerScreen = &Screen2;
 Button4.Order = 3;
 Button4.Left = 43;
 Button4.Top = 150;
 Button4.Width = 83;
 Button4.Height = 72;
 Button4.Pen_Width = 1;
 Button4.Pen_Color = 0x0000;
 Button4.Visible = 1;
 Button4.Active = 1;
 Button4.Transparent = 1;
 Button4.Caption = Button4_Caption;
 Button4.TextAlign = _taCenter;
 Button4.TextAlignVertical= _tavMiddle;
 Button4.FontName = Tahoma11x13_Regular;
 Button4.PressColEnabled = 1;
 Button4.Font_Color = 0x0000;
 Button4.VerticalText = 0;
 Button4.Gradient = 1;
 Button4.Gradient_Orientation = 0;
 Button4.Gradient_Start_Color = 0x3660;
 Button4.Gradient_End_Color = 0xC618;
 Button4.Color = 0xC618;
 Button4.Press_Color = 0xE71C;
 Button4.OnUpPtr = Button4Onrelease;
 Button4.OnDownPtr = Button4Onpush;
 Button4.OnClickPtr = 0;
 Button4.OnPressPtr = 0;

 Button5.OwnerScreen = &Screen2;
 Button5.Order = 4;
 Button5.Left = 196;
 Button5.Top = 150;
 Button5.Width = 83;
 Button5.Height = 72;
 Button5.Pen_Width = 1;
 Button5.Pen_Color = 0x0000;
 Button5.Visible = 1;
 Button5.Active = 1;
 Button5.Transparent = 1;
 Button5.Caption = Button5_Caption;
 Button5.TextAlign = _taCenter;
 Button5.TextAlignVertical= _tavMiddle;
 Button5.FontName = Tahoma11x13_Regular;
 Button5.PressColEnabled = 1;
 Button5.Font_Color = 0x0000;
 Button5.VerticalText = 0;
 Button5.Gradient = 1;
 Button5.Gradient_Orientation = 0;
 Button5.Gradient_Start_Color = 0xC980;
 Button5.Gradient_End_Color = 0xC618;
 Button5.Color = 0xC618;
 Button5.Press_Color = 0xE71C;
 Button5.OnUpPtr = Button5Onrelease;
 Button5.OnDownPtr = Button5Onpush;
 Button5.OnClickPtr = 0;
 Button5.OnPressPtr = 0;

 Button6.OwnerScreen = &Screen2;
 Button6.Order = 5;
 Button6.Left = 350;
 Button6.Top = 150;
 Button6.Width = 83;
 Button6.Height = 72;
 Button6.Pen_Width = 1;
 Button6.Pen_Color = 0x0000;
 Button6.Visible = 1;
 Button6.Active = 1;
 Button6.Transparent = 1;
 Button6.Caption = Button6_Caption;
 Button6.TextAlign = _taCenter;
 Button6.TextAlignVertical= _tavMiddle;
 Button6.FontName = Tahoma11x13_Regular;
 Button6.PressColEnabled = 1;
 Button6.Font_Color = 0x0000;
 Button6.VerticalText = 0;
 Button6.Gradient = 1;
 Button6.Gradient_Orientation = 0;
 Button6.Gradient_Start_Color = 0x6013;
 Button6.Gradient_End_Color = 0xC618;
 Button6.Color = 0xC618;
 Button6.Press_Color = 0xE71C;
 Button6.OnUpPtr = 0;
 Button6.OnDownPtr = 0;
 Button6.OnClickPtr = Button6Onclick;
 Button6.OnPressPtr = 0;
}

static char IsInsideObject (unsigned int X, unsigned int Y, unsigned int Left, unsigned int Top, unsigned int Width, unsigned int Height) {
 if ( (Left<= X) && (Left+ Width - 1 >= X) &&
 (Top <= Y) && (Top + Height - 1 >= Y) )
 return 1;
 else
 return 0;
}





void DrawButton(TButton *Abutton) {
unsigned int ALeft, ATop;
 if (Abutton->Visible != 0) {
 if (object_pressed == 1) {
 object_pressed = 0;
 TFT_Set_Brush(Abutton->Transparent, Abutton->Press_Color, Abutton->Gradient, Abutton->Gradient_Orientation, Abutton->Gradient_End_Color, Abutton->Gradient_Start_Color);
 }
 else {
 TFT_Set_Brush(Abutton->Transparent, Abutton->Color, Abutton->Gradient, Abutton->Gradient_Orientation, Abutton->Gradient_Start_Color, Abutton->Gradient_End_Color);
 }
 TFT_Set_Pen(Abutton->Pen_Color, Abutton->Pen_Width);
 TFT_Rectangle(Abutton->Left, Abutton->Top, Abutton->Left + Abutton->Width - 1, Abutton->Top + Abutton->Height - 1);
 if (Abutton->VerticalText != 0)
 TFT_Set_Font(Abutton->FontName, Abutton->Font_Color, FO_VERTICAL_COLUMN);
 else
 TFT_Set_Font(Abutton->FontName, Abutton->Font_Color, FO_HORIZONTAL);
 TFT_Write_Text_Return_Pos(Abutton->Caption, Abutton->Left, Abutton->Top);
 if (Abutton->TextAlign == _taLeft)
 ALeft = Abutton->Left + 4;
 else if (Abutton->TextAlign == _taCenter)
 ALeft = Abutton->Left + (Abutton->Width - caption_length) / 2;
 else if (Abutton->TextAlign == _taRight)
 ALeft = Abutton->Left + Abutton->Width - caption_length - 4;

 if (Abutton->TextAlignVertical == _tavTop)
 ATop = Abutton->Top + 4;
 else if (Abutton->TextAlignVertical == _tavMiddle)
 ATop = Abutton->Top + ((Abutton->Height - caption_height) / 2);
 else if (Abutton->TextAlignVertical == _tavBottom)
 ATop = Abutton->Top + (Abutton->Height - caption_height - 4);

 TFT_Write_Text(Abutton->Caption, ALeft, ATop);
 }
}

void DrawImage(TImage *AImage) {
 if (AImage->Visible != 0) {
 TFT_Image_Jpeg(AImage->Left, AImage->Top, AImage->Picture_Name);
 }
}

void DrawScreen(TScreen *aScreen) {
 unsigned int order;
 unsigned short button_idx;
 TButton *local_button;
 unsigned short image_idx;
 TImage *local_image;
 char save_bled, save_bled_direction;

 object_pressed = 0;
 order = 0;
 button_idx = 0;
 image_idx = 0;
 CurrentScreen = aScreen;

 if ((display_width != CurrentScreen->Width) || (display_height != CurrentScreen->Height)) {
 save_bled = TFT_BLED;
 save_bled_direction = TFT_BLED_Direction;
 TFT_BLED_Direction = 0;
 TFT_BLED = 0;
 TFT_Init_SSD1963(CurrentScreen->Width, CurrentScreen->Height);
 STMPE610_SetSize(CurrentScreen->Width, CurrentScreen->Height);
 TFT_Fill_Screen(CurrentScreen->Color);
 display_width = CurrentScreen->Width;
 display_height = CurrentScreen->Height;
 TFT_BLED = save_bled;
 TFT_BLED_Direction = save_bled_direction;
 }
 else
 TFT_Fill_Screen(CurrentScreen->Color);


 while (order < CurrentScreen->ObjectsCount) {
 if (button_idx < CurrentScreen->ButtonsCount) {
 local_button =  CurrentScreen->Buttons[button_idx] ;
 if (order == local_button->Order) {
 button_idx++;
 order++;
 DrawButton(local_button);
 }
 }

 if (image_idx < CurrentScreen->ImagesCount) {
 local_image =  CurrentScreen->Images[image_idx] ;
 if (order == local_image->Order) {
 image_idx++;
 order++;
 DrawImage(local_image);
 }
 }

 }
}

void Get_Object(unsigned int X, unsigned int Y) {
 button_order = -1;
 image_order = -1;

 for ( _object_count = 0 ; _object_count < CurrentScreen->ButtonsCount ; _object_count++ ) {
 local_button =  CurrentScreen->Buttons[_object_count] ;
 if (local_button->Active != 0) {
 if (IsInsideObject(X, Y, local_button->Left, local_button->Top,
 local_button->Width, local_button->Height) == 1) {
 button_order = local_button->Order;
 exec_button = local_button;
 }
 }
 }


 for ( _object_count = 0 ; _object_count < CurrentScreen->ImagesCount ; _object_count++ ) {
 local_image =  CurrentScreen->Images[_object_count] ;
 if (local_image->Active != 0) {
 if (IsInsideObject(X, Y, local_image->Left, local_image->Top,
 local_image->Width, local_image->Height) == 1) {
 image_order = local_image->Order;
 exec_image = local_image;
 }
 }
 }

 _object_count = -1;
 if (button_order > _object_count )
 _object_count = button_order;
 if (image_order > _object_count )
 _object_count = image_order;
}


void Process_TP_Press(unsigned int X, unsigned int Y) {
 exec_button = 0;
 exec_image = 0;

 Get_Object(X, Y);

 if (_object_count != -1) {
 if (_object_count == button_order) {
 if (exec_button->Active != 0) {
 if (exec_button->OnPressPtr != 0) {
 exec_button->OnPressPtr();
 return;
 }
 }
 }

 if (_object_count == image_order) {
 if (exec_image->Active != 0) {
 if (exec_image->OnPressPtr != 0) {
 exec_image->OnPressPtr();
 return;
 }
 }
 }

 }
}

void Process_TP_Up(unsigned int X, unsigned int Y) {

 switch (PressedObjectType) {

 case 0: {
 if (PressedObject != 0) {
 exec_button = (TButton*)PressedObject;
 if ((exec_button->PressColEnabled != 0) && (exec_button->OwnerScreen == CurrentScreen)) {
 DrawButton(exec_button);
 }
 break;
 }
 break;
 }
 }

 exec_image = 0;

 Get_Object(X, Y);


 if (_object_count != -1) {

 if (_object_count == button_order) {
 if (exec_button->Active != 0) {
 if (exec_button->OnUpPtr != 0)
 exec_button->OnUpPtr();
 if (PressedObject == (void *)exec_button)
 if (exec_button->OnClickPtr != 0)
 exec_button->OnClickPtr();
 PressedObject = 0;
 PressedObjectType = -1;
 return;
 }
 }


 if (_object_count == image_order) {
 if (exec_image->Active != 0) {
 if (exec_image->OnUpPtr != 0)
 exec_image->OnUpPtr();
 if (PressedObject == (void *)exec_image)
 if (exec_image->OnClickPtr != 0)
 exec_image->OnClickPtr();
 PressedObject = 0;
 PressedObjectType = -1;
 return;
 }
 }

 }
 PressedObject = 0;
 PressedObjectType = -1;
}

void Process_TP_Down(unsigned int X, unsigned int Y) {

 object_pressed = 0;
 exec_button = 0;
 exec_image = 0;

 Get_Object(X, Y);

 if (_object_count != -1) {
 if (_object_count == button_order) {
 if (exec_button->Active != 0) {
 if (exec_button->PressColEnabled != 0) {
 object_pressed = 1;
 DrawButton(exec_button);
 }
 PressedObject = (void *)exec_button;
 PressedObjectType = 0;
 if (exec_button->OnDownPtr != 0) {
 exec_button->OnDownPtr();
 return;
 }
 }
 }

 if (_object_count == image_order) {
 if (exec_image->Active != 0) {
 PressedObject = (void *)exec_image;
 PressedObjectType = 3;
 if (exec_image->OnDownPtr != 0) {
 exec_image->OnDownPtr();
 return;
 }
 }
 }

 }
}

void Check_TP() {
 if (STMPE610_PressDetect()) {
 if (STMPE610_GetLastCoordinates(&Xcoord, &Ycoord) == 0) {

 Process_TP_Press(Xcoord, Ycoord);
 if (PenDown == 0) {
 PenDown = 1;
 Process_TP_Down(Xcoord, Ycoord);
 }
 }
 }
 else if (PenDown == 1) {
 PenDown = 0;
 Process_TP_Up(Xcoord, Ycoord);
 }
}

void Init_MCU()
{
 OSCCON=0x33E0;
 CLKDIV=0x0006;
 PLLFBD=278;
 ANSELD=0x0000;
 ANSELA=0x0000;
}

char STMPE610_Config() {
 I2C2_Init(100000);
 STMPE610_SetI2CAddress(STMPE610_I2C_ADDR1);
 if (STMPE610_IsOperational() != 0){
 return STMPE610_IO_NOT_OPERATIONAL;
 }

 STMPE610_Reset();
 STMPE610_Module(STMPE610_MODULE_TS | STMPE610_MODULE_ADC, STMPE610_ENABLE);
 STMPE610_AlternateFunction(STMPE610_GPIO_PIN1, STMPE610_ENABLE);
 STMPE610_SetGPIOPin(STMPE610_GPIO_PIN1, 0);
 STMPE610_SetSize(480,272);
 STMPE610_Module(STMPE610_MODULE_TS | STMPE610_MODULE_ADC, STMPE610_ENABLE);
 STMPE610_ConfigureInterrupt(STMPE610_INT_STOP_ALL);
 STMPE610_SetADC(STMPE610_ADC_CTRL1_SAMPLETIME_44 | STMPE610_ADC_CTRL1_ADC_12BIT | STMPE610_ADC_CTRL1_INT_REFERENCE);
 Delay_10ms(); Delay_10ms();
 STMPE610_SetADCClock(STMPE610_ADC_CTRL2_3250_kHZ);
 STMPE610_AlternateFunction(STMPE610_GPIO_PIN4 | STMPE610_GPIO_PIN5 | STMPE610_GPIO_PIN6 | STMPE610_GPIO_PIN7, STMPE610_DISABLE);
 STMPE610_ConfigureTSC(STMPE610_TSC_CFG_AVE_CTRL_4S, STMPE610_TSC_CFG_TOUCH_DET_DELAY_500uS, STMPE610_TSC_CFG_TOUCH_SETTLING_500uS);
 STMPE610_SetFIFOThreshold(1);
 STMPE610_ResetFIFO();
 STMPE610_TSIDrive(STMPE610_TSC_I_DRIVE_20mA);
 STMPE610_TSControl(STMPE610_TSC_CTRL_TRACK0 | STMPE610_TSC_CTRL_ACQU_XYZ | STMPE610_TSC_CTRL_ENABLE);
 STMPE610_ZDataFraction(STMPE610_FRACP4_WHOLP4);
 STMPE610_SetTouchPressureThreshold(70);
 STMPE610_ClearInterrupts();
 STMPE610_WriteReg(STMPE610_INT_CTRL_REG, 0x01);
 return STMPE610_OK;
}


void Start_TP() {
TTPConstants TPConstsStruct;
 Init_MCU();

 InitializeTouchPanel();
 STMPE610_Config();
 if (STMPE610_Config() == STMPE610_OK) {
 } else {
 while(1);
 }



 TPConstsStruct.X_Min = 105;
 TPConstsStruct.X_Max = 3925;
 TPConstsStruct.Y_Min = 250;
 TPConstsStruct.Y_Max = 3820;
 TPConstsStruct.Rotate = 0;
 STMPE610_SetCalibrationConsts(&TPConstsStruct);

 InitializeObjects();
 display_width = Screen1.Width;
 display_height = Screen1.Height;
 DrawScreen(&Screen1);
 TFT_Set_DBC_SSD1963(255);
 Delay_ms(2000);

}
