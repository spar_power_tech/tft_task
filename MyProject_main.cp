#line 1 "C:/Users/James/Desktop/SPARR/SPAR/Program/Demo_touch_back_up - Copy/mikroC/MyProject_main.c"
#line 1 "c:/users/james/desktop/sparr/spar/program/demo_touch_back_up - copy/mikroc/myproject_objects.h"
typedef enum {_taLeft, _taCenter, _taRight} TTextAlign;
typedef enum {_tavTop, _tavMiddle, _tavBottom} TTextAlignVertical;

typedef struct Screen TScreen;

typedef struct {
 unsigned int X_Min;
 unsigned int X_Max;
 unsigned int Y_Min;
 unsigned int Y_Max;
 char Rotate;
} TTPConstants;

typedef struct Button {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 char Pen_Width;
 unsigned int Pen_Color;
 char Visible;
 char Active;
 char Transparent;
 char *Caption;
 TTextAlign TextAlign;
 TTextAlignVertical TextAlignVertical;
 const far char *FontName;
 unsigned int Font_Color;
 char VerticalText;
 char Gradient;
 char Gradient_Orientation;
 unsigned int Gradient_Start_Color;
 unsigned int Gradient_End_Color;
 unsigned int Color;
 char PressColEnabled;
 unsigned int Press_Color;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TButton;

typedef struct Image {
 TScreen* OwnerScreen;
 char Order;
 unsigned int Left;
 unsigned int Top;
 unsigned int Width;
 unsigned int Height;
 const far char *Picture_Name;
 char Visible;
 char Active;
 char Picture_Type;
 char Picture_Ratio;
 void (*OnUpPtr)();
 void (*OnDownPtr)();
 void (*OnClickPtr)();
 void (*OnPressPtr)();
} TImage;

struct Screen {
 unsigned int Color;
 unsigned int Width;
 unsigned int Height;
 unsigned int ObjectsCount;
 unsigned int ButtonsCount;
 TButton * const code far *Buttons;
 unsigned int ImagesCount;
 TImage * const code far *Images;
};

extern TScreen Screen1;
extern TImage Image1;
extern TImage * const code far Screen1_Images[1];

extern TScreen Screen2;
extern TButton Button1;
extern TButton Button2;
extern TButton Button3;
extern TButton Button4;
extern TButton Button5;
extern TButton Button6;
extern TButton * const code far Screen2_Buttons[6];




void Button1OnClick();
void Button2Onclick();
void Button3Onpush();
void Button3Onrelease();
void Button4Onpush();
void Button4Onrelease();
void Button5Onpush();
void Button5Onrelease();
void Button6Onclick();
void Image1Onclick();




extern char Image1_Caption[];
extern char Button1_Caption[];
extern char Button2_Caption[];
extern char Button3_Caption[];
extern char Button4_Caption[];
extern char Button5_Caption[];
extern char Button6_Caption[];


void DrawScreen(TScreen *aScreen);
void DrawButton(TButton *aButton);
void DrawImage(TImage *AImage);
void Check_TP();
void Start_TP();
void Process_TP_Press(unsigned int X, unsigned int Y);
void Process_TP_Up(unsigned int X, unsigned int Y);
void Process_TP_Down(unsigned int X, unsigned int Y);
#line 30 "C:/Users/James/Desktop/SPARR/SPAR/Program/Demo_touch_back_up - Copy/mikroC/MyProject_main.c"
unsigned short normallyOpenClose, DOVAL, tempDO;



unsigned short DIVAL, NI2CD, tempDI, inputChange;
extern unsigned short testvar;
void EXPANDER_write(char chipxx, char portAddr, char dat){
 I2C2_Start();

 I2C2_Write(chipxx);
 I2C2_Write(portAddr);
 I2C2_Write(dat);
 I2C2_Stop();
}
char EXPANDER_read(char chipxx, char portAddr) {
 char dat;
 I2C2_Start();

 I2C2_Write(chipxx);
 I2C2_Write(portAddr);
 I2C2_restart();
 I2C2_Write(chipxx+1);
 dat = I2C2_Read(1);
 I2C_Stop();
 return dat;
}
void EXPANDER_init(char portA, char portB, char portC){
 EXPANDER_write ( 0x40 ,  0x00 , portA);
 EXPANDER_write ( 0x40 ,  0x01 , portB);
 EXPANDER_write ( 0x42 ,  0x00 , portC);
 EXPANDER_write ( 0x42 , 0x01 , 0xFF );
 }


void INIT1_interrupt(){
 EXPANDER_write( 0x42 , 0x02, 0xFF );
 EXPANDER_write( 0x42 , 0x03, 0x00 );
 EXPANDER_write( 0x42 , 0x04, 0x00 );
 }


void I2C_int1Chip2() iv IVT_ADDR_INT1INTERRUPT {

 NI2CD=1;
#line 83 "C:/Users/James/Desktop/SPARR/SPAR/Program/Demo_touch_back_up - Copy/mikroC/MyProject_main.c"
 IFS1.INT1IF = 0;
}
void main() {
 Start_TP();

 ANSELB = 0x01DF;
 TRISC = 0xFFFF;
 ANSELC = 0x0000;
 TRISE = 0xFE5F;
 ANSELE = 0x0000;
 TRISF = 0x0104;
 TRISG = 0xFFFF;
 ANSELG = 0x0000;
 LATE.b8 = 1;
 LATF.b3=1;

 Unlock_IOLOCK();
 PPS_Mapping_NoLock(89, _INPUT, _INT1);
 Lock_IOLOCK();

 NI2CD=1;
 DIVAL =0;
 I2C2_init(100000);
 EXPANDER_init(0x00, 0xFF, 0xFF);
 EXPANDER_write( 0x42 , 0x02, 0xFF);
 EXPANDER_write( 0x42 , 0x03, 0x00);
 EXPANDER_write( 0x42 , 0x04, 0x00);
 EXPANDER_write( 0x42 , 0x05, 0x02);

 IPC5=IPC5 | 0x0007;
 IFS1.INT1IF = 0;
 IEC1.INT1IE = 1;



 Start_TP();

 while (1) {

 Check_TP();

 }


}
